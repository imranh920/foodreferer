
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:foodrefer/Screens/Shared/text_styles.dart';

class ConnectSocialView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: true,
        backgroundColor: Colors.white,
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.black),
          onPressed: () {
            // Write some code to control things, when user press back button in AppBar
            moveToLastScreen(context);
          },
        ),
      ),
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.all( 20.0),

        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Choose an account',
                style: headerStyle,),
              SizedBox(height: 20),
              SizedBox(height: 20),
              SizedBox(height: 20),
              Container(
                height: 40.0,
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1.0),
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Image(
                          image: AssetImage(
                              'lib/assets/social_icons/fb_logo.png'),
                          height: 30.0,
                          width: 30.0,
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Center(
                        child: Text('Log in with facebook',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat')),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              Container(
                height: 40.0,
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1.0),
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Image(
                          image: AssetImage(
                              'lib/assets/social_icons/google_logo.png'),
                          height: 25.0,
                          width: 25.0,
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Center(
                        child: Text('Log in with Google',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat')),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              Container(
                height: 40.0,
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(
                          color: Colors.black,
                          style: BorderStyle.solid,
                          width: 1.0),
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(20.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(
                        child: Image(
                          image: AssetImage(
                              'lib/assets/social_icons/twitter_logo.png'),
                          height: 50.0,
                          width: 50.0,
                        ),
                      ),
                      SizedBox(width: 10.0),
                      Center(
                        child: Text('Log in with Twitter',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Montserrat')),
                      )
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
            ],
          ),
          alignment: Alignment(-1.0, 0.0),

        ),

      ),
    );
  }

  void moveToLastScreen(context) {
    Navigator.pop(context);

  }
}
