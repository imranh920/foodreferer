import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'button.dart';

class Counter extends StatefulWidget{

  final maxValue;
  final TextEditingController counterController;
  Counter({Key key, @required this.counterController,this.maxValue}) : super(key:key);

  _CounterState createState()=> _CounterState();

}

class _CounterState extends State<Counter>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Container(
              height: 40.0,
              width: 40.0,
              child: Button(color:Colors.green[700],child: Icon(
                Icons.remove,
                color: Colors.white,
                size: 20.0,
              ),shape:RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25.0),
                  bottomLeft: Radius.circular(25.0),
                ),
              ) ,onTap: (){UpdateCounter(widget.counterController,-1);},),
            ),
          ),
          Expanded(
            child: Container(
              /*decoration: BoxDecoration(
                color: Colors.white,
                border: Border.all(color: Colors.black),
              ),*/
              color: Colors.white,

              height: 40.0,
              width: 50.0,
              child: TextField(
                controller: widget.counterController,
                enabled: false,
                /*controller: counterController,
                inputFormatters:<TextInputFormatter> [
                  WhitelistingTextInputFormatter.digitsOnly,
                  BlacklistingTextInputFormatter(RegExp("^[0]")),
                ],*/
                style: TextStyle(fontSize: 15.0,color: Colors.black,),
                decoration: new InputDecoration(contentPadding: const EdgeInsets.all(10.0),border: InputBorder.none,),
                keyboardType:TextInputType.numberWithOptions(decimal: true),
              ),
            ),
          ),
          Expanded(
            child: Container(
              height: 40.0,
              width: 40.0,
              child: Button(color:Colors.green[700],child: Icon(
                Icons.add,
                color: Colors.white,
                size: 20.0,
              ),shape:RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(25.0),
                  bottomRight: Radius.circular(25.0),),) ,
                onTap: (){UpdateCounter(widget.counterController,1);},
              ),
            ),
          ),
        ],
      ),
    );
  }

  void UpdateCounter(controller,int val){
    setState(() {
      int currentVal =  int.parse(controller.text);
      int newVal = (currentVal + val);
      if(newVal>(widget.maxValue??0)){
        controller.text= newVal.toString();
      }
    });
  }
}