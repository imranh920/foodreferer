import 'package:flutter/material.dart';

class AppIcon extends StatelessWidget {

  Widget getLogo() {
    AssetImage assetImage = AssetImage('lib/assets/logo/logo.png');
    Image image = Image(
      image: assetImage,
      width: 150.0,
      height: 150.0,
    );

    return Container(

        child:Center(
          child: image,
        )

    );
  }
  @override
  Widget build(BuildContext context) {
    return getLogo();
  }
}
