import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Button extends StatefulWidget{

  final Function onTap;
  final ShapeBorder shape;
  final Color color;
  final Widget child;
  Button({Key key, @required this.child, this.color,this.shape,this.onTap}) : super(key:key);

  _ButtonState createState()=> _ButtonState();

}

class _ButtonState extends State<Button>{

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return RawMaterialButton(
      shape: widget.shape??RoundedRectangleBorder(),
      onPressed: () {
        widget.onTap();
      },
      child:widget.child,
      elevation: 5.0,
      fillColor: widget.color??Colors.blue[400],
    );
  }
}