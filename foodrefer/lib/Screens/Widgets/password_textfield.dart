import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PasswordField extends StatefulWidget{
  TextEditingController _passwordController;
  PasswordField(this._passwordController);
  @override
  PasswordFieldState createState() => new PasswordFieldState(_passwordController);
}

class PasswordFieldState extends State<PasswordField>{

  TextEditingController _passwordController;
  PasswordFieldState(this._passwordController);

  @override
  Widget build(BuildContext context) {
    return Container(
      child:TextField(
        controller: _passwordController,
        obscureText: true,
        style: TextStyle(color: Colors.green.shade900),
        decoration: InputDecoration(
          prefixIcon: Icon(
            Icons.lock,
            color: Colors.green.shade900,
          ),
          hintText: "Password",
          hintStyle: TextStyle(color: Colors.green.shade900),
          enabledBorder: const OutlineInputBorder(
            borderSide: const BorderSide(color: Colors.green, width: 0.5),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.green, width: 2.0),
          ),
        ),
      ));
  }
}