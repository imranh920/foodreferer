import 'package:flutter/material.dart';

class OfferCardItem extends StatelessWidget {
  final String _price;
  OfferCardItem(this._price);
  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery.of(context).size.width;
    double screen_height = MediaQuery.of(context).size.height;
    return Container(
      key: Key(_price),
      width: screen_width,
      decoration: new BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 0.99),
      ),
      padding: EdgeInsets.all(7),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Container(
            height: screen_height * 0.29,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage(
                    "lib/assets/images/res" + 1.toString() + ".jpg"),
                fit: BoxFit.fill,
              ),
              borderRadius: new BorderRadius.all(Radius.circular(7)),
            ),
            padding: const EdgeInsets.all(8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: screen_height * 0.198,
                ),
                Container(
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: screen_height * 0.06,
                        width: screen_width * 0.34,
                        decoration: new BoxDecoration(
                          color: Color.fromRGBO(108, 89, 220, 0.99),
                          borderRadius:
                              new BorderRadius.all(Radius.circular(7)),
                        ),
                      ),
                      Positioned(
                        left: 10,
                        bottom: 7,
                        child: Icon(
                          Icons.call_missed_outgoing,
                          color: Colors.white70,
                        ),
                      ),
                      Positioned(
                        left: 35,
                        bottom: 12,
                        child: Text("TRENDING",
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                letterSpacing: 1.3,
                                fontWeight: FontWeight.w800)),
                      )
                    ],
                  ),
                ),
                SizedBox(
                  height: 5,
                )
              ],
            ),
          ),
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Devil Dawgs",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 22),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "4.8",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 17),
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber[300],
                      size: 20,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber[300],
                      size: 20,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber[300],
                      size: 20,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber[300],
                      size: 20,
                    ),
                    Icon(
                      Icons.star,
                      color: Colors.amber[300],
                      size: 20,
                    ),
                    Text(
                      "(129)",
                      style:
                          TextStyle(fontWeight: FontWeight.w400, fontSize: 17),
                    )
                  ],
                ),
                SizedBox(
                  height: 6,
                ),
                Text(
                  "767 South State Street, Chicago",
                  style: TextStyle(fontWeight: FontWeight.w400, fontSize: 17),
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      '\$' + _price,
                      style: TextStyle(
                          fontWeight: FontWeight.w900,
                          fontSize: 27,
                          color: Colors.green),
                    ),
                    SizedBox(
                      width: 7,
                    ),
                    Stack(
                      children: <Widget>[
                        Container(
                          height: screen_height * 0.04,
                          width: screen_width * 0.25,
                          decoration: new BoxDecoration(
                            color: Colors.green[200],
                            borderRadius:
                                new BorderRadius.all(Radius.circular(7)),
                          ),
                        ),
                        Positioned(
                          left: 10,
                          bottom: 5.1,
                          child: Text("35%  ",
                              style: TextStyle(
                                  color: Colors.green[800],
                                  letterSpacing: 1.3,
                                  fontWeight: FontWeight.w800)),
                        ),
                        Positioned(
                          left: 45,
                          bottom: 5.1,
                          child: Text("OFF",
                              style: TextStyle(
                                  color: Colors.green[800],
                                  letterSpacing: 1.3,
                                  fontWeight: FontWeight.w800)),
                        )
                      ],
                    ),
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
