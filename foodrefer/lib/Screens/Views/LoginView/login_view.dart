import 'dart:io';
import 'package:flutter/material.dart';
import 'package:foodrefer/Screens/Views/BaseView/base_view.dart';
import 'package:foodrefer/Screens/Views/CustomerSpecificViews/HomeView/customer_home_view.dart';
import 'package:foodrefer/Screens/Widgets/app_icon.dart';
import 'package:foodrefer/Screens/Widgets/button.dart';
import 'package:foodrefer/Screens/Widgets/generic_textfield.dart';
import 'package:foodrefer/Screens/Widgets/password_textfield.dart';
import 'package:foodrefer/ViewModels/LoginModels/login_model.dart';
import 'package:foodrefer/core/DependencyRegister/locator.dart';
import 'package:foodrefer/core/Helper/validators.dart';
import 'package:foodrefer/core/Models/user.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => new _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final GlobalKey<FormState> _loginformKey = GlobalKey<FormState>();
  var rememberMe = false;
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery.of(context).size.width;
    double screen_height = MediaQuery.of(context).size.height;
    return BaseView<LoginModel>(
        builder: (context, model, child) => WillPopScope(
              onWillPop: () => _onBackPressed(context, model),
              child: Scaffold(
                  key: _scaffoldKey,
                  appBar: AppBar(
                    title: Text('Log In'),
                    automaticallyImplyLeading: false,
                  ),
                  resizeToAvoidBottomPadding: false,
                  backgroundColor: Colors.white,
                  body: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                          padding: EdgeInsets.only(top: 30.0),
                          child: Container(
                              height: screen_height * 0.25, child: AppIcon())),
                      Expanded(
                          child: Container(
                              padding: EdgeInsets.only(
                                  top: 20.0,
                                  left: 20.0,
                                  right: 20.0,
                                  bottom: 20.0),
                              child: Form(
                                key: _loginformKey,
                                child: ListView(
                                  children: <Widget>[
                                    GenericTextField(
                                      model.emailTextController,
                                      "Email or Username",
                                      Icons.email,
                                      validators: Validators.validateIsEmpty,
                                    ),
                                    SizedBox(height: 20.0),
                                    PasswordField(model.passwordController),
                                    SizedBox(height: 20.0),
                                    model.loginTapped == false
                                        ? Container(
                                            height: 50.0,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                              boxShadow: [
                                                BoxShadow(
                                                  color: Colors.amberAccent,
                                                  blurRadius:
                                                      15.0, // soften the shadow
                                                  spreadRadius:
                                                      -18.0, //extend the shadow
                                                  offset: Offset(
                                                    0.0, // Move to right 10  horizontally
                                                    20.0, // Move to bottom 10 Vertically
                                                  ),
                                                )
                                              ],
                                            ),
                                            child: Button(
                                              child: Center(
                                                child: Text(
                                                  'LOGIN',
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontFamily: 'Montserrat'),
                                                ),
                                              ),
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          20.0)),
                                              onTap: () {
                                                _validateInputs(model);
                                              },
                                            ),
                                          )
                                        : Center(
                                            child: CircularProgressIndicator(
                                                valueColor:
                                                    AlwaysStoppedAnimation(
                                                        Colors.green[900]),
                                                strokeWidth: 5.0)),
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Expanded(
                                          flex: 2,
                                          child: Container(
                                              child: CheckboxListTile(
                                            controlAffinity:
                                                ListTileControlAffinity.leading,
                                            title: Text("remember me"),
                                            checkColor: Colors.green[900],
                                            // color of tick Mark
                                            activeColor: Colors.green,
                                            value: rememberMe,
                                            onChanged: (bool value) {
                                              setState(() {
                                                rememberMe = value;
                                              });
                                            },
                                          )),
                                        ),
                                        Expanded(
                                          flex: 1,
                                          child: Container(
                                              child: forgotPasswordField),
                                        )
                                      ],
                                    ),
                                    /*InkWell(
                                      onTap: (){Navigator.pushNamed(context, '/connect_social');},
                                      child: Container(
                                        child: Padding(
                                          padding: EdgeInsets.only(
                                              left: 10, top: 10.0),
                                          child: Text(
                                            'Or connect with social account',
                                            style: TextStyle(
                                                color: Colors.green[900],
                                                fontSize: 15,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      ),
                                    ),*/
                                    SizedBox(
                                      height: 10.0,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'Do not have an account?',
                                          style: TextStyle(
                                            fontSize: 15,
                                          ),
                                        ),
                                        InkWell(
                                          onTap: () {
                                            Navigator.pushNamed(
                                                context, '/sign_up');
                                          },
                                          child: Container(
                                            child: Text(
                                              'Sign Up',
                                              style: TextStyle(
                                                  color: Colors.green[900],
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    Row(children: <Widget>[
                                      Expanded(child: Divider()),
                                      Text(
                                        'Or connect with social account',
                                        style: TextStyle(
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Expanded(child: Divider()),
                                    ]),
                                    SizedBox(
                                      height: 15.0,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceAround,
                                      children: <Widget>[
                                        BuidSocialLoginButton(
                                            "lib/assets/social_icons/fb_logo.png",
                                            model.loginWithFacebook),
                                        BuidSocialLoginButton(
                                            "lib/assets/social_icons/google_logo.png",
                                            model.loginWithGoogle),
                                        BuidSocialLoginButton(
                                            "lib/assets/social_icons/twitter_logo.png",
                                            null),
                                      ],
                                    ),
                                  ],
                                ),
                              ))),
                    ],
                  )),
            ));
  }

  InkWell BuidSocialLoginButton(String assetPath, Function onTapCallback) {
    return InkWell(
        child: Container(
          height: 50.0,
          width: 50.0,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage(assetPath), fit: BoxFit.cover)),
        ),
        onTap: () async {
          var usr = await onTapCallback(context);
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => CustomerHomeView(usr)),
          );
        });
  }

  final forgotPasswordField = Material(
    child: InkWell(
      onTap: () {},
      child: Text(
        'Forgot Password',
        style: TextStyle(
            color: Colors.green,
            fontWeight: FontWeight.bold,
            fontFamily: 'Montserrat',
            decoration: TextDecoration.underline),
      ),
    ),
  );
  final snackBar = SnackBar(
    content: Text('Invalid Username/Password!'),
  );
  void _validateInputs(LoginModel model) {
    setState(() {
      if (_loginformKey.currentState.validate()) {
        model.loginTapped = true;
        _loginformKey.currentState.save();
        model
            .login(
                model.emailTextController.text, model.passwordController.text)
            .then((user) {
          if (user != null) {
            model.saveCurrentUserInfo(user);
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CustomerHomeView(user)),
            );
          } else {
            _scaffoldKey.currentState.showSnackBar(snackBar);
            setState(() {
              model.loginTapped = false;
            });
          }
        });
      } else {}
    });
  }

  Future<bool> _onBackPressed(context, model) {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Do you want to close the app?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('No'),
                  onPressed: () => Navigator.pop(context, false),
                ),
                FlatButton(
                  child: Text('Yes'),
                  onPressed: () {
                    model.closeApp();
                  },
                )
              ],
            ));
  }
}
