import 'dart:async';
import 'package:flutter/material.dart';
import 'package:foodrefer/Screens/Views/BaseView/base_view.dart';
import 'package:foodrefer/Screens/Views/CustomerSpecificViews/category_view.dart';
import 'package:foodrefer/Screens/Views/CustomerSpecificViews/offers_view.dart';
import 'package:foodrefer/Screens/Widgets/badge_icon.dart';
import 'package:foodrefer/ViewModels/CustomerHomeModel/customer_home_model.dart';
import 'package:foodrefer/core/Models/user.dart';

class CustomerHomeView extends StatefulWidget {
  final User user;
  CustomerHomeView(this.user);

  @override
  _CustomerHomeViewState createState() => new _CustomerHomeViewState(user);
}

class _CustomerHomeViewState extends State<CustomerHomeView>
    with TickerProviderStateMixin {
  final User user;
  _CustomerHomeViewState(this.user);

  StreamController<int> notificationCountController =
      StreamController<int>.broadcast();

  TextEditingController _searchQueryController = TextEditingController();
  TabController _tabController;
  bool _isSearching = false;

  String searchQuery = "Search query";
  int _currentIndex = 0;
  int count = 1;
  void initState() {
    // TODO: implement initState
    notificationCountController.close();
    _tabController = new TabController(vsync: this, length: 2);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery.of(context).size.width;
    double screen_height = MediaQuery.of(context).size.height;
    return BaseView<CustomerHomeModel>(
        builder: (context, model, child) => WillPopScope(
              onWillPop: () {},
              child: Scaffold(
                appBar: PreferredSize(
                    preferredSize: Size.fromHeight(108),
                    child: AppBar(
                      title: Text(
                        'REFERER',
                        style: TextStyle(
                            fontWeight: FontWeight.bold, letterSpacing: 2),
                      ),
                      actions: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(9.0),
                          child: PopupMenuButton(
                            child: StreamBuilder(
                                initialData: count,
                                stream: notificationCountController.stream,
                                builder: (context, snapshot) {
                                  return BadgeIcon(
                                    icon: Icon(Icons.shopping_cart,
                                        color: Colors.white, size: 25),
                                    badgeCount: snapshot.data,
                                  );
                                }),
                            itemBuilder: (BuildContext context) =>
                                ["Cart Item 1"]
                                    .map((item) => PopupMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                          ),
                                        ))
                                    .toList(),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(9.0),
                          child: PopupMenuButton(
                            child: StreamBuilder(
                                initialData: count,
                                stream: notificationCountController.stream,
                                builder: (context, snapshot) {
                                  return BadgeIcon(
                                    icon: Icon(Icons.notifications,
                                        color: Colors.white, size: 25),
                                    badgeCount: snapshot.data,
                                  );
                                }),
                            itemBuilder: (BuildContext context) =>
                                ["You got a new referral!"]
                                    .map((item) => PopupMenuItem<String>(
                                          value: item,
                                          child: Text(
                                            item,
                                          ),
                                        ))
                                    .toList(),
                          ),
                        )
                      ],
                      bottom: PreferredSize(
                        preferredSize: Size.fromHeight(70.0),
                        child: Container(
                          height: 70,
                          color: Colors.white,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: <Widget>[
                              Container(
                                height: 38,
                                padding: EdgeInsets.all(0),
                                child: _buildSearchField(),
                              ),
                              PreferredSize(
                                  preferredSize: Size.fromHeight(32.0),
                                  child: TabBar(
                                    controller: _tabController,
                                    tabs: <Widget>[
                                      _createTopMenuItem("All Offers"),
                                      _createTopMenuItem("Categories")
                                    ],
                                  ))
                            ],
                          ),
                        ),
                      ),
                    )),
                drawer: Drawer(
                    child: ListView(
                  padding: EdgeInsets.zero,
                  children: <Widget>[
                    _createHeader(screen_height),
                  ],
                )),
                body: Stack(
                  children: <Widget>[
                    Positioned.fill(
                      child: Container(
                          decoration: new BoxDecoration(
                              image: new DecorationImage(
                            image: new AssetImage("lib/assets/images/food.jpg"),
                            fit: BoxFit.cover,
                          )),
                          child: new TabBarView(
                              controller: _tabController,
                              children: [OffersView(), CategoryView()])),
                    )
                  ],
                ),
                bottomNavigationBar: BottomNavigationBar(
                  currentIndex: _currentIndex, // new
                  items: [
                    new BottomNavigationBarItem(
                      icon: Icon(Icons.home),
                      title: Text('Home'),
                    ),
                    new BottomNavigationBarItem(
                      icon: Icon(Icons.mail),
                      title: Text('Messages'),
                    ),
                    new BottomNavigationBarItem(
                        icon: Icon(Icons.person), title: Text('Profile'))
                  ],
                ),
              ),
            ));
  }

  Widget _getRowItem(index, text) {
    return Container(
      padding: EdgeInsets.fromLTRB(5, 4, 8, 4),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.black87)),
        color: Color.fromRGBO(255, 255, 255, 0.99),
      ),
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
              flex: 9,
              child: Container(
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        text + " (" + ((index + 1) * 100).toString() + ")",
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: Colors.black54,
                            fontSize: 20))),
              )),
          Expanded(
            flex: 1,
            child: Padding(
                padding: EdgeInsets.all(8),
                child: Icon(
                  Icons.control_point,
                )),
          )
        ],
      ),
    );
  }

  Widget _getCategoryTile(index) {
    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage(
              "lib/assets/images/tile" + ((index % 4) + 1).toString() + ".jpg"),
          fit: BoxFit.cover,
        ),
        borderRadius: new BorderRadius.all(Radius.circular(20)),
      ),
      padding: const EdgeInsets.all(8),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 25,
          ),
          Icon(Icons.card_giftcard),
          Text("Offer!", style: TextStyle(fontWeight: FontWeight.w500)),
          SizedBox(
            height: 5,
          ),
          Text(
            "Click Here!",
            style: TextStyle(fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }

  Widget _createTopMenuItem(String text) {
    return Container(
        decoration: BoxDecoration(color: Colors.blueAccent[50]),
        padding: EdgeInsets.all(5),
        child: Center(
            child: Text(
          text,
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
        )));
  }

  Widget _buildSearchField() {
    return Center(
        child: TextField(
      controller: _searchQueryController,
      decoration: new InputDecoration(
        contentPadding: EdgeInsets.all(5),
        prefixIcon: Icon(
          Icons.search,
          color: Colors.greenAccent,
        ),
        border: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.green, width: 1.0),
        ),
        hintText: 'Search Data',
      ),
      style: TextStyle(
        color: Colors.black87,
        fontSize: 16.0,
      ),
      autofocus: false,
      onChanged: (query) => {},
    ));
  }

  Widget _createHeader(screen_height) {
    return Container(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        height: screen_height * 0.35,
        color: Colors.amber[300],
        child: Stack(children: <Widget>[
          Align(
            child: Container(
                child: Icon(
              Icons.account_circle,
              color: Colors.grey,
              size: 100.0,
            )),
          ),
          Positioned(
              bottom: 12.0,
              left: 60.0,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(user.user_nicename,
                      style: TextStyle(
                          color: Colors.green,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500)),
                  SizedBox(
                    height: 5.0,
                  ),
                  Text(user.user_email,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 15.0,
                      ))
                ],
              )),
        ]));
  }
}
