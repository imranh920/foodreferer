import 'package:flutter/material.dart';
import 'package:foodrefer/Screens/Widgets/item_card.dart';

class CategoryView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery.of(context).size.width;
    double screen_height = MediaQuery.of(context).size.height;
    // TODO: implement build
    return Container(
        child: ListView(
      children: <Widget>[
        SizedBox(
          height: 5,
        ),
        Container(
            height: screen_height / 2,
            decoration: BoxDecoration(
              borderRadius: new BorderRadius.all(Radius.circular(15)),
              color: Color.fromRGBO(255, 255, 255, 0.99),
            ),
            child: GridView.count(
              primary: false,
              padding: const EdgeInsets.all(10),
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              crossAxisCount: 3,
              children: List.generate(20, (index) {
                return _getCategoryTile(index);
              }),
            )),
        SizedBox(
          height: 5,
        ),
        //Middle Container

        SizedBox(
          height: 5,
        ),
        Container(
          decoration: BoxDecoration(
            borderRadius: new BorderRadius.all(Radius.circular(15)),
            color: Color.fromRGBO(255, 255, 255, 0.99),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.only(left: 8),
                child: Text(
                  "All Categories",
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 22),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(8),
                width: screen_width,
                color: Colors.black12,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "LOCAL",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          letterSpacing: 2),
                    )),
              ),
              Column(
                  children: List.generate(12, (index) {
                return _getRowItem(index, "Food & Drink");
              })),
              SizedBox(
                height: 10,
              ),
              Container(
                padding: EdgeInsets.all(8),
                width: screen_width,
                color: Colors.black12,
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "GOODS",
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 20,
                          letterSpacing: 2),
                    )),
              ),
              Column(
                  children: List.generate(10, (index) {
                return _getRowItem(index, "Personal Services");
              }))
            ],
          ),
        )
      ],
    ));
  }

  Widget _getCategoryTile(index) {
    return Container(
      decoration: new BoxDecoration(
        image: new DecorationImage(
          image: new AssetImage(
              "lib/assets/images/tile" + ((index % 4) + 1).toString() + ".jpg"),
          fit: BoxFit.cover,
        ),
        borderRadius: new BorderRadius.all(Radius.circular(20)),
      ),
      padding: const EdgeInsets.all(8),
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 25,
          ),
          Icon(Icons.card_giftcard),
          Text("Offer!", style: TextStyle(fontWeight: FontWeight.w500)),
          SizedBox(
            height: 5,
          ),
          Text(
            "Click Here!",
            style: TextStyle(fontWeight: FontWeight.w500),
          )
        ],
      ),
    );
  }

  Widget _getRowItem(index, text) {
    return Container(
      padding: EdgeInsets.fromLTRB(5, 4, 8, 4),
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(color: Colors.black87)),
        color: Color.fromRGBO(255, 255, 255, 0.99),
      ),
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Expanded(
              flex: 9,
              child: Container(
                child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                        text + " (" + ((index + 1) * 100).toString() + ")",
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: Colors.black54,
                            fontSize: 20))),
              )),
          Expanded(
            flex: 1,
            child: Padding(
                padding: EdgeInsets.all(8),
                child: Icon(
                  Icons.control_point,
                )),
          )
        ],
      ),
    );
  }
}
