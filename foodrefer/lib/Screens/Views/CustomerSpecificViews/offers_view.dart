import 'package:flutter/material.dart';
import 'package:foodrefer/Screens/Widgets/item_card.dart';

class OffersView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double screen_width = MediaQuery.of(context).size.width;
    double screen_height = MediaQuery.of(context).size.height;

    // TODO: implement build
    return Container(
        child: ListView(
      children: getItems(),
    ));
  }

  getItems() {
    List<Widget> items = List<Widget>();
    items.add(SizedBox(
      height: 5,
    ));
    List.generate(15, (index) {
      items.add(OfferCardItem(((index + 1) * 10).toString()));
      items.add(Container(
          color: Colors.black12,
          child: SizedBox(
            height: 5,
          )));
    });
    return items;
  }
}
