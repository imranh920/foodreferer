import 'package:flutter/material.dart';

const Color backgroundColor = Color.fromARGB(255, 255, 241, 159);
const Color commentColor = Color.fromARGB(255, 255, 246, 196);
const Color primaryAppThemeColor = Color.fromARGB(255, 243, 84, 26);