import 'package:flutter/cupertino.dart';
import 'package:foodrefer/ViewModels/BaseModel/base_model.dart';
import 'package:foodrefer/core/DependencyRegister/locator.dart';
import 'package:foodrefer/core/Enums/enums.dart';
import 'package:foodrefer/core/Helper/sharedpref.dart';
import 'package:foodrefer/core/Models/user.dart';
import 'package:foodrefer/core/Services/auth_service/auth_service.dart';
import 'package:foodrefer/core/Services/user_service/user_service.dart';

class LoginModel extends BaseModel {
  final UserService _userService = locator<UserService>();
  final AuthenticationService _authService = locator<AuthenticationService>();
  TextEditingController emailTextController=TextEditingController();
  TextEditingController passwordController=TextEditingController();
  String errorMessage;
  bool loginTapped=false;

  Future<User> login(String email,String password) async {
    setState(ViewState.Busy);
    User user =  await _userService.login(email,password);
    setState(ViewState.Idle);
    return user;
  }

  void saveCurrentUserInfo(User user){
    _userService.setCurrentUser(user);
  }

  void closeApp(){
    _userService.closeApp();
  }
  Future<User> loginWithFacebook(BuildContext context) async {
    var profile = await _authService.initiateFacebookLogin();
    
      if(profile == null){

      } else {
        var user = User(user_display_name: profile["name"],user_nicename: profile["name"], user_email: profile["email"], user_id: profile["id"]);
        saveCurrentUserInfo(user);
        return user;
        
      }
    
  }
  Future<User> loginWithGoogle(BuildContext context) async {
    var profile = await _authService.initiateGoogleLogin();
    
      if(profile is bool){
        return null;
      } else {
        var user = User(user_display_name: profile.displayName,user_nicename: profile.displayName, user_email: profile.email, user_id: profile.id);
        saveCurrentUserInfo(user);
        return user;
      }
    
  }
}