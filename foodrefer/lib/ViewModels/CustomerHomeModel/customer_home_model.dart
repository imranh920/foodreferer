import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:foodrefer/ViewModels/BaseModel/base_model.dart';
import 'package:foodrefer/core/DependencyRegister/locator.dart';

class CustomerHomeModel extends BaseModel{
  
  StreamController<int> notificationCountController = StreamController<int>.broadcast();
  int notificationCount=1;
}