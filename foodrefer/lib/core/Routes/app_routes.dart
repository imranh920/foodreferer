
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:foodrefer/Screens/Views/CustomerSpecificViews/HomeView/customer_home_view.dart';
import 'package:foodrefer/Screens/Views/LoginView/login_view.dart';
import 'package:foodrefer/core/Models/user.dart';

const String initialRoute = "login";

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginView());
      case '/customer_home':
        return MaterialPageRoute(builder: (_) => CustomerHomeView(User()));
      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
              body: Center(
                child: Text('No route defined for ${settings.name}'),
              ),
            ));
    }
  }
}
