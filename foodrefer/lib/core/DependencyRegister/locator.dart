import 'package:foodrefer/ViewModels/BaseModel/base_model.dart';
import 'package:foodrefer/ViewModels/CustomerHomeModel/customer_home_model.dart';
import 'package:foodrefer/ViewModels/LoginModels/login_model.dart';
import 'package:foodrefer/core/Services/auth_service/auth_service.dart';
import 'package:foodrefer/core/Services/user_service/user_service.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator() {
  //services
  locator.registerLazySingleton(() => UserService());
  locator.registerLazySingleton(() => AuthenticationService());
  //models
  locator.registerFactory(() => BaseModel());
  locator.registerFactory(() => LoginModel());
  locator.registerFactory(() => CustomerHomeModel());
}
